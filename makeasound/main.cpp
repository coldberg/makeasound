#include <iostream>
#include <cassert>
#include <complex>
#include <vector>
#include <algorithm>

#define SDL_MAIN_HANDLED
#include <SDL.h>

// A very important constant for music :)
const float pi = 3.14159265359f;

int main(int, char**)
{
	// Initialize the black magic
	SDL_Init(SDL_INIT_EVERYTHING);

	// Open a window to get key presses from
	auto window_ = SDL_CreateWindow(nullptr, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280u, 720u, 0u);

	// Must not fail!
	assert(window_ != nullptr);

	// Specify audio characteristics we want
	SDL_AudioSpec desired_ = 
	{ 
		48000,		// 48KHz sample rate	 
		AUDIO_F32,	// floating point samples
		1,			// 1 channle, mono		
		0,			//    unimportant
		512,		// 512 samples per buffer
		0,			//    unimportant, leave 0
		0,			//    unimportant, leave 0
		nullptr,	//    unimportant, leave nullptr
		nullptr		//    unimportant, leave nullptr
	};

	// This will contain the characteristics we actually got
	SDL_AudioSpec obtained_ = { };

	// Open audio device
	auto device_id_ = SDL_OpenAudioDevice(nullptr, 0, &desired_, &obtained_, 0);

	// Must not fail
	assert(device_id_ != 0);

	// Create a buffer with the number of samples for one chunk
	std::vector<float> buff_(obtained_.samples);

	// The duration of one sample
	const auto period_ = 1.0f / obtained_.freq;
	// Current global phase
	auto phase_ = 0.0f;
	// Note frequency
	auto note_frequency_ = 440.0f;

	// Note envelope phase
	auto env_phase_ = 0.0f;
	// Note playing
	auto note_on_ = false;

	// Number of chunks to buffer, the higher , the more delay
	const auto nchunks_ = 2;

	// Number of keys pressed
	auto numkeys_ = 0;

	// Decay speed
	const auto decay_ = 4.0f;

	for(;;)
	{
		// Pump events, and catch key strokes here
		SDL_Event event_;
		if (SDL_PollEvent(&event_))
		{
			if (event_.type == SDL_QUIT)
				break;

			// Play note

			switch (event_.type)
			{
				case SDL_KEYDOWN:
				{
					// Don't care about repeats
					if (event_.key.repeat != 0)
						continue;

					// Count keys pressed, to not abruptly stop the note when going key to key
					++numkeys_;

					switch (event_.key.keysym.sym)
					{
					// C-Major on the numeric keys :)
					case SDLK_1:		note_frequency_ = 440.f * std::pow(2.0f,  0.0f / 12.0f); break; // C4
					case SDLK_2:		note_frequency_ = 440.f * std::pow(2.0f,  2.0f / 12.0f); break; // D4
					case SDLK_3:		note_frequency_ = 440.f * std::pow(2.0f,  4.0f / 12.0f); break; // E4
					case SDLK_4:		note_frequency_ = 440.f * std::pow(2.0f,  5.0f / 12.0f); break; // F4
					case SDLK_5:		note_frequency_ = 440.f * std::pow(2.0f,  7.0f / 12.0f); break; // G4
					case SDLK_6:		note_frequency_ = 440.f * std::pow(2.0f,  9.0f / 12.0f); break; // A5
					case SDLK_7:		note_frequency_ = 440.f * std::pow(2.0f, 11.0f / 12.0f); break; // B5

					case SDLK_8:		note_frequency_ = 880.f * std::pow(2.0f,  0.0f / 12.0f); break; // C5
					case SDLK_9:		note_frequency_ = 880.f * std::pow(2.0f,  2.0f / 12.0f); break; // D5
					case SDLK_0:		note_frequency_ = 880.f * std::pow(2.0f,  4.0f / 12.0f); break; // E5
					case SDLK_MINUS:	note_frequency_ = 880.f * std::pow(2.0f,  5.0f / 12.0f); break; // G5
					case SDLK_EQUALS:	note_frequency_ = 880.f * std::pow(2.0f,  7.0f / 12.0f); break; // A6
					}

					// Reset phase
					phase_ = 0.0f;
					// Reset evelope
					env_phase_ = 0.0f;
					// Note on
					note_on_ = true;
					continue;
				}

				// Stop note
				case SDL_KEYUP:
				{
					--numkeys_;

					// only stop if no keys are pressed
					note_on_ = numkeys_ != 0;

					continue;
				}

			}
			continue;
		}

		// If we have less then N chunks of audio queued up, make a new one
		if (SDL_GetQueuedAudioSize(device_id_) < obtained_.size * nchunks_)
		{
			// Generate the samples
			for (auto& s : buff_)
			{
				// Envelope generation https://www.wolframalpha.com/input/?i=plot+min(ln(t*8+%2B+1),+exp(-t))++where+t+%3E%3D+0
				const auto envelope_ = std::min (std::log(env_phase_*8.0f + 1), std::exp(-env_phase_));
				// Multiply the sinusoide by envelope and by 1.0f or 0.0f depending on note on or off
				s = std::sin(phase_ * 2.0f * pi) * envelope_ * (note_on_ ? 1.0f : 0.0f);

				// Advance phases for the oscillator
				phase_ += period_ * note_frequency_;

				// Advance phases for the envelope
				env_phase_ += period_ * decay_;
			}

			// Send the audio data to the queue
			SDL_QueueAudio(device_id_, buff_.data(), buff_.size() * sizeof(buff_[0]));

			// If the device isn't playing , play
			if (SDL_GetAudioDeviceStatus(device_id_) != SDL_AUDIO_PLAYING)
			{
				SDL_PauseAudioDevice(device_id_, 0);
			}
		}
	}

	// Close the device
	SDL_CloseAudioDevice(device_id_);

	// Destroy the window
	SDL_DestroyWindow(window_);

	// RAGE QUIT!
	SDL_Quit();
	return 0;
}